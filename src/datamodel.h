#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QObject>
#include <QStringListModel>
#include <QVector>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>

class DataModel : public QObject {
    Q_OBJECT
public:
    DataModel(QObject* parent = nullptr);
    QPieSeries* pieSeries(const QString& key, int group_factor);
    std::pair<QStringList, QBarSeries*> barSeries(const QString& key, int group_factor);
    QSplineSeries* splineSeries(const QString& key);
    bool validateKey(const QString& key);
    QStringListModel* keysModel() { return keys_; }

private:
    QString filename_;
    QHash<QString, QVector<double>> values_;
    QStringListModel* keys_;
    QVector<int> groupFactors(int group_factor, long long container_size);

public slots:
    void setDataset();
signals:
    void modelChanged(QString dataset_folder);
};

#endif // DATAMODEL_H
