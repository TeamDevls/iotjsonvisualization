#include "datamodel.h"

#include <QBrush>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>

DataModel::DataModel(QObject* parent)
    : QObject(parent)
{
    values_ = QHash<QString, QVector<double>>();
    keys_ = new QStringListModel(this);
}

QPieSeries* DataModel::pieSeries(const QString& key, int group_factor)
{
    QMap<double, int> frequency_map;
    for (auto& each : values_[key])
        frequency_map[each]++;

    auto series = new QPieSeries();
    QMapIterator<double, int> it(frequency_map);

    for (auto& each_factor : groupFactors(group_factor, frequency_map.size())) {

        auto group_minimum = 0.0;
        auto group_current_value = 0.0;
        for (auto i = 0; i < each_factor; i++) {
            it.next();
            if (i == 0)
                group_minimum = it.key();
            group_current_value += it.value();
        }
        auto group_current_category = group_minimum == it.key() ? QString::number(group_minimum)
                                                                : QString("[%1 - %2]").arg(group_minimum).arg(it.key());
        series->append(group_current_category, group_current_value);
    }

    return series;
}

std::pair<QStringList, QBarSeries*> DataModel::barSeries(const QString& key, int group_factor)
{
    QMap<double, int> frequency_map;
    for (auto& each : values_[key])
        frequency_map[each]++;

    auto categories = QStringList();
    auto bar_set = new QBarSet(QString("Frequencies of \"%1\" values").arg(key));
    bar_set->setBrush(QBrush(QColor(251, 192, 45)));

    auto series = new QBarSeries();

    QMapIterator<double, int> it(frequency_map);

    for (auto& each_factor : groupFactors(group_factor, frequency_map.size())) {

        auto group_minimum = 0.0;
        auto group_current_value = 0.0;
        for (auto i = 0; i < each_factor; i++) {
            it.next();
            if (i == 0)
                group_minimum = it.key();
            group_current_value += it.value();
        }
        auto group_current_category = group_minimum == it.key() ? QString::number(group_minimum)
                                                                : QString("[%1 - %2]").arg(group_minimum).arg(it.key());
        categories << group_current_category;
        *bar_set << group_current_value;
    }

    series->append(bar_set);
    bar_set->setLabelColor(Qt::black);
    series->setLabelsPosition(QAbstractBarSeries::LabelsOutsideEnd);
    series->setLabelsVisible(true);
    return { categories, series };
}

QSplineSeries* DataModel::splineSeries(const QString& key)
{
    auto series = new QSplineSeries();
    series->setName(QString("Graphic of \"%1\" changes").arg(key));
    auto x = values_["timestamp"];
    auto y = values_[key];
    auto xs = x.size();
    for (long long i = 0; i < xs; i++)
        series->append(x[i], y[i]);
    series->setColor(QColor(230, 81, 0));
    return series;
}

QVector<int> DataModel::groupFactors(int group_factor, long long container_size)
{
    if (group_factor > container_size || group_factor == 0)
        return QVector<int>(container_size, 1);
    else {
        QVector<int> group_factors(group_factor, container_size / group_factor);
        auto mod = container_size % group_factor;
        for (auto i = 0; i < mod; i++)
            group_factors[i]++;
        return group_factors;
    }
}

bool DataModel::validateKey(const QString& key)
{
    if (values_.contains(key))
        return true;

    QMessageBox::critical(
        qobject_cast<QWidget*>(parent()),
        "Error",
        QString("Current dataset doesn't contains field \"%1\"").arg(key));

    return false;
}

void DataModel::setDataset()
{
    auto filePath = QFileDialog::getOpenFileName(qobject_cast<QWidget*>(parent()),
        tr("Open Json Dataset"), "C:/User/", tr("Json File (*.json)"));
    if (filePath != "") {
        QFile jsonFile(filePath);
        if (!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qWarning("Couldn't open save file");
        } else {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonFile.readAll());
            jsonFile.close();
            auto jsonArray = jsonDocument.toVariant().toMap()["data"].toList();

            for (auto& each : values_) {
                each.clear();
                each.shrink_to_fit();
            }

            for (auto& jsonElement : jsonArray) {
                auto map = jsonElement.toMap();
                values_["timestamp"].append(map["timestamp"].toDateTime().toMSecsSinceEpoch());
                map.erase(map.find("timestamp"));

                QMapIterator<QString, QVariant> it(map);
                while (it.hasNext()) {
                    it.next();
                    values_[it.key()].append(it.value().toDouble());
                }
            }

            keys_->setStringList(values_.keys());
            emit modelChanged(filePath);
        }
    }
}
