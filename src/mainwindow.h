#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCompleter>
#include <QDateTimeAxis>
#include <QWidget>
#include <datamodel.h>

class MainWindow : public QWidget {
    Q_OBJECT
public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private:
    DataModel* model_;
    QChartView* chart_views_[3];
    QChart* charts_[3];
    QValueAxis* axisX[3];
    QValueAxis* axisY[3];
    //QCustomPlot* plot_;
};
#endif // MAINWINDOW_H
