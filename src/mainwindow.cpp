#include "mainwindow.h"
#include <QBarCategoryAxis>
#include <QIntValidator>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QScrollBar>
#include <QVBoxLayout>
#include <QtCharts/QPieLegendMarker>

MainWindow::MainWindow(QWidget* parent)
    : QWidget(parent)
{
    model_ = new DataModel(this);
    auto layout = new QVBoxLayout();
    setLayout(layout);
    setStyleSheet("* {border:0px;border-radius: 10px;}"
                  "QLabel {background-color: white;}"
                  "QPushButton {background-color: white;}"
                  "QMessageBox {background: white;}"
                  "QPushButton::hover {background-color: rgb(220, 220, 220)}");

    auto scrollArea = new QScrollArea;
    scrollArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    scrollArea->setWidgetResizable(true);
    auto charts_frame = new QWidget();
    auto charts_layout = new QVBoxLayout();
    charts_frame->setLayout(charts_layout);
    scrollArea->setWidget(charts_frame);
    scrollArea->setAlignment(Qt::Alignment(Qt::AlignHCenter | Qt::AlignTop));
    scrollArea->horizontalScrollBar()->setFixedSize(0, 0);
    scrollArea->verticalScrollBar()->setFixedSize(0, 0);
    scrollArea->setVerticalScrollBarPolicy(Qt ::ScrollBarAlwaysOff);
    layout->addWidget(scrollArea);

    auto json_label = new QLabel();
    json_label->setAlignment(Qt::AlignCenter);
    json_label->hide();
    auto open_json_btn = new QPushButton("Open json dataset");
    open_json_btn->setMinimumHeight(open_json_btn->sizeHint().height() * 1.2);
    connect(
        open_json_btn,
        &QPushButton::clicked,
        model_,
        &DataModel::setDataset);

    chart_views_[0] = new QChartView(this);
    chart_views_[0]->setRenderHint(QPainter::Antialiasing);
    // Добавляем его в горизонтальный Layout

    //Создаем GUI гистограммы
    charts_[0] = new QChart();
    charts_[0]->setAnimationOptions(QChart::SeriesAnimations);
    charts_[0]->setTitle("Circle Graphic");
    charts_[0]->legend()->show();
    charts_[0]->legend()->setAlignment(Qt::AlignRight);
    //series->attachAxis(axisY);

    //Создаем GUI кругового графика
    chart_views_[1] = new QChartView(this);
    chart_views_[1]->setRenderHint(QPainter::Antialiasing);

    charts_[1] = new QChart();
    charts_[1]->setAnimationOptions(QChart::SeriesAnimations);
    charts_[1]->setTitle("Bar graphic");

    //Создаем GUI дистограммы
    chart_views_[2] = new QChartView(this);
    chart_views_[2]->setRenderHint(QPainter::Antialiasing);

    charts_[2] = new QChart();
    charts_[2]->setAnimationOptions(QChart::SeriesAnimations);
    charts_[2]->setTitle("Curve graphic");

    auto pieHBoxLayout = new QHBoxLayout();
    auto pieQLineEdit = new QLineEdit();
    pieQLineEdit->setMinimumHeight(pieQLineEdit->sizeHint().height() * 1.2);
    pieQLineEdit->setAlignment(Qt::AlignCenter);
    pieQLineEdit->setPlaceholderText("Type something then choose from the list below");

    QCompleter* pieKeysCompleter = new QCompleter(model_->keysModel(), this);
    pieKeysCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    pieKeysCompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    pieQLineEdit->setCompleter(pieKeysCompleter);

    auto pieGroupFactorLine = new QLineEdit();
    pieGroupFactorLine->setMinimumHeight(pieGroupFactorLine->sizeHint().height() * 1.2);
    pieGroupFactorLine->setAlignment(Qt::AlignCenter);
    pieGroupFactorLine->setPlaceholderText("Enter group factor >= 1");
    auto pieGroupFactorValidator = new QIntValidator(pieGroupFactorLine);
    pieGroupFactorValidator->setBottom(1);
    pieGroupFactorLine->setValidator(pieGroupFactorValidator);
    auto piePushButton = new QPushButton("Plot it!");
    piePushButton->setMinimumSize(piePushButton->sizeHint().width() * 1.2, piePushButton->sizeHint().height() * 1.2);
    piePushButton->setEnabled(false);
    connect(piePushButton,
        &QPushButton::clicked,
        [this, pieQLineEdit, pieGroupFactorLine]() {
            if (auto text = pieQLineEdit->text(); text != "") {
                if (model_->validateKey(text)) {
                    charts_[0]->removeAllSeries();
                    for (auto& each : charts_[0]->axes())
                        charts_[0]->removeAxis(each);
                    auto group_factor = pieGroupFactorLine->hasAcceptableInput() ? pieGroupFactorLine->text().toInt()
                                                                                 : 0;
                    auto pieSeries = model_->pieSeries(text, group_factor);
                    charts_[0]->addSeries(pieSeries);

                    const auto markers = charts_[0]->legend()->markers(pieSeries);
                    for (QLegendMarker* marker : markers) {
                        QPieLegendMarker* pieMarker = qobject_cast<QPieLegendMarker*>(marker);
                        pieMarker->setLabel(QString("%1 - %2 (%3%)")
                                                .arg(pieMarker->slice()->label())
                                                .arg(pieMarker->slice()->value())
                                                .arg(pieMarker->slice()->percentage() * 100, 0, 'f', 2));
                    }
                }
            }
        });
    pieHBoxLayout->addStretch(1);
    pieHBoxLayout->addWidget(pieQLineEdit, 3);
    pieHBoxLayout->addWidget(pieGroupFactorLine, 1);
    pieHBoxLayout->addWidget(piePushButton, 1);
    pieHBoxLayout->addStretch(1);

    auto barHBoxLayout = new QHBoxLayout();
    auto barQLineEdit = new QLineEdit();
    barQLineEdit->setMinimumHeight(barQLineEdit->sizeHint().height() * 1.2);
    barQLineEdit->setAlignment(Qt::AlignCenter);
    barQLineEdit->setPlaceholderText("Type something then choose from the list below");

    QCompleter* barKeysCompleter = new QCompleter(model_->keysModel(), this);
    barKeysCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    barKeysCompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    barQLineEdit->setCompleter(barKeysCompleter);

    auto barGroupFactorLine = new QLineEdit();
    barGroupFactorLine->setMinimumHeight(barGroupFactorLine->sizeHint().height() * 1.2);
    barGroupFactorLine->setAlignment(Qt::AlignCenter);
    barGroupFactorLine->setPlaceholderText("Enter group factor >= 1");
    auto barGroupFactorValidator = new QIntValidator(barGroupFactorLine);
    barGroupFactorValidator->setBottom(1);
    barGroupFactorLine->setValidator(barGroupFactorValidator);
    auto barPushButton = new QPushButton("Plot it!");
    barPushButton->setMinimumSize(barPushButton->sizeHint().width() * 1.2, barPushButton->sizeHint().height() * 1.2);
    barPushButton->setEnabled(false);
    connect(barPushButton,
        &QPushButton::clicked,
        [this, barQLineEdit, barGroupFactorLine]() {
            if (auto text = barQLineEdit->text(); text != "") {
                if (model_->validateKey(text)) {
                    charts_[1]->removeAllSeries();
                    for (auto& each : charts_[1]->axes())
                        charts_[1]->removeAxis(each);

                    auto group_factor = barGroupFactorLine->hasAcceptableInput() ? barGroupFactorLine->text().toInt()
                                                                                 : 0;

                    auto [categories, bar_series] = model_->barSeries(text, group_factor);
                    charts_[1]->addSeries(bar_series);
                    QBarCategoryAxis* axisX = new QBarCategoryAxis();
                    axisX->setCategories(categories);
                    axisX->setTitleText("Values");
                    charts_[1]->addAxis(axisX, Qt::AlignBottom);
                    bar_series->attachAxis(axisX);
                    QValueAxis* axisY = new QValueAxis();
                    axisY->setTitleText("Values frequency");
                    charts_[1]->addAxis(axisY, Qt::AlignLeft);
                    bar_series->attachAxis(axisY);
                    axisY->setMax(axisY->max() * 1.1);
                }
            }
        });
    barHBoxLayout->addStretch(1);
    barHBoxLayout->addWidget(barQLineEdit, 3);
    barHBoxLayout->addWidget(barGroupFactorLine, 1);
    barHBoxLayout->addWidget(barPushButton, 1);
    barHBoxLayout->addStretch(1);

    auto curveHBoxLayout = new QHBoxLayout();
    auto curveQLineEdit = new QLineEdit();
    curveQLineEdit->setAlignment(Qt::AlignCenter);
    curveQLineEdit->setMinimumHeight(curveQLineEdit->sizeHint().height() * 1.2);
    curveQLineEdit->setPlaceholderText("Type something then choose from the list below");

    QCompleter* curveKeysCompleter = new QCompleter(model_->keysModel(), this);
    curveKeysCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    curveKeysCompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    curveQLineEdit->setCompleter(curveKeysCompleter);

    auto curvePushButton = new QPushButton("Plot it!");
    curvePushButton->setMinimumSize(curvePushButton->sizeHint().width() * 1.2, curvePushButton->sizeHint().height() * 1.2);
    curvePushButton->setEnabled(false);
    connect(curvePushButton,
        &QPushButton::clicked,
        [this, curveQLineEdit]() {
            if (auto text = curveQLineEdit->text(); text != "") {
                if (model_->validateKey(text)) {
                    charts_[2]->removeAllSeries();
                    for (auto& each : charts_[2]->axes())
                        charts_[2]->removeAxis(each);

                    auto spline_series = model_->splineSeries(text);
                    charts_[2]->addSeries(spline_series);
                    QDateTimeAxis* axisX = new QDateTimeAxis;
                    axisX->setTickCount(10);
                    axisX->setFormat("hh:mm:ss");
                    axisX->setTitleText("Date");
                    charts_[2]->addAxis(axisX, Qt::AlignBottom);
                    spline_series->attachAxis(axisX);
                    QValueAxis* axisY = new QValueAxis();
                    charts_[2]->addAxis(axisY, Qt::AlignLeft);
                    spline_series->attachAxis(axisY);
                    axisY->setTitleText("Value");
                    axisY->setTickCount(8);
                    axisY->setRange(0, axisY->max() * 1.1);
                }
            }
        });
    curveHBoxLayout->addStretch();
    curveHBoxLayout->addWidget(curveQLineEdit);
    curveHBoxLayout->addWidget(curvePushButton);
    curveHBoxLayout->addStretch();

    // Устанавливаем график в представление

    chart_views_[0]->setChart(charts_[0]);
    charts_layout->addWidget(chart_views_[0]);
    charts_layout->addLayout(pieHBoxLayout);
    chart_views_[0]->setMinimumHeight(rect().height());

    chart_views_[1]->setChart(charts_[1]);
    charts_layout->addWidget(chart_views_[1]);
    charts_layout->addLayout(barHBoxLayout);
    chart_views_[1]->setMinimumHeight(rect().height());

    chart_views_[2]->setChart(charts_[2]);
    charts_layout->addWidget(chart_views_[2]);
    charts_layout->addLayout(curveHBoxLayout);
    chart_views_[2]->setMinimumHeight(rect().height());

    auto manage_layout = new QHBoxLayout;
    layout->addLayout(manage_layout);
    manage_layout->addWidget(json_label);
    manage_layout->addWidget(open_json_btn);

    connect(model_,
        &DataModel::modelChanged,
        [json_label, open_json_btn, curvePushButton, piePushButton, barPushButton](QString filename) {
            json_label->setText(QString("Dataset: %1").arg(filename));
            json_label->show();
            open_json_btn->setText("Load another json dataset");
            curvePushButton->setEnabled(true);
            piePushButton->setEnabled(true);
            barPushButton->setEnabled(true);
        });
}

MainWindow::~MainWindow()
{
}
