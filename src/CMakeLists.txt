#it's required to hide app console window behind gui when app starts outside the IDE(only windows)
if (${CMAKE_SYSTEM} MATCHES "Windows")
    set(CMAKE_WIN32_EXECUTABLE ON)
endif()

set(CMAKE_INCLUDE_CURRENT_DIR ON)

#find_package(QT NAMES Qt6 Qt5 COMPONENTS Gui Widgets Core REQUIRED)
find_package(Qt6 COMPONENTS REQUIRED Gui Widgets Core Charts)

file(GLOB_RECURSE HEADERS *.h)
file(GLOB_RECURSE SOURCES *.cpp)
file(GLOB_RECURSE QRC *.qrc)

add_executable(${CMAKE_PROJECT_NAME} ${SOURCES} ${HEADERS} ${QRC})
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE Qt6::Widgets Qt6::Gui Qt6::Core Qt6::Charts)
